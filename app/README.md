 # Explanation

This sample project illustrates the use of Upload Api (accepts all types of file) as well as delete and retrieve files.

Use prod env when deploying the app and ensure the application is running as intended after deployed. Please provide dockerfile/docker-compose file.

## Upload file

Method: POST

http://localhost:8080/api/v1/upload

## Delete file

Method: DELETE

http://localhost:8080/api/v1/upload/filename

## Retrieve file

Method: GET

http://localhost:8080/api/v1/retrieve/filename



http://localhost:8080/api/v1/retrieve/abc.txt

curl -X POST -d "testing post via curl" http://localhost:8080/api/v1/p/2.txt


curl -d  --data-binary @"/tmp/test.txt" http://localhost:8080/api/v1/upload

curl -X POST -d "testing post via curl" http://18.136.199.34:8080/api/v1/upload/post